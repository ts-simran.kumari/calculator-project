
package calculator;

public class ScientificCalculator implements BasicCalculator1{

    @Override
    public double getMemory() {
        return memory;
    }

    @Override
    public void setMemory(double c) {
    }

    @Override
    public double NthRoot(double x, double y) {
        y=1/y;
        return Math.pow(x,y);
    }

    @Override
    public double pow(double x, double y) {
        return Math.pow(x,y);
    }

    @Override
    public double sin(double x) {
        double radian= Math.toRadians(x);
        return Math.sin(radian);
    }

    @Override
    public double cos(double x) {
        double radian= Math.toRadians(x);
        return Math.cos(radian);
    }

    @Override
    public double tan(double x) {
        double radian= Math.toRadians(x);
        return Math.tan(radian);
    }

    @Override
    public double log(double x) {
        return Math.log10(x);
    }

    @Override
    public double Add(double x, double y) {
        return 0;
    }

    @Override
    public double subtraction(double x, double y) {
        return 0;
    }

    @Override
    public double Multiplication(double x, double y) {
        return 0;
    }

    @Override
    public double Divison(double x, double y) {
        return 0;
    }

    @Override
    public double Modulus(double x, double y) {
        return 0;
    }

    @Override
    public double clearMemory() {
        return 0;
    }
}