
package calculator;

public class StandardCalculator implements BasicCalculator {


    @Override
    public  double Add(double x, double y) {
        return x+y;
    }

    @Override
    public double subtraction(double x, double y) {
       return x-y;
    }

    @Override
    public double Multiplication(double x, double y) {
        return x*y;
    }

    @Override
    public double Divison(double x, double y) {
        return x/y;
    }

    @Override
    public double Modulus(double x, double y) {
        return x%y;
    }

    @Override
    public double clearMemory() {
        return 0;
    }


}
