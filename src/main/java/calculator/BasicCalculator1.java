package calculator;

public interface BasicCalculator1 extends BasicCalculator {
    double memory=0;
    double getMemory();
    void setMemory(double c);
    double NthRoot(double x,double y);
    double pow(double x, double y);
    double sin(double x);
    double cos(double x);
    double tan(double x);
    double log(double x);

}
