import calculator.*;

import java.util.Scanner;
import java.lang.*;


public class Calculator {
    public static void main(String args[]) {

        Scanner input = new Scanner(System.in);
        System.out.println("enter mode:\n1 for standard calculator\n 2 for scientific calculator\n 3 for generalized calculator");
        char mode = input.next().charAt(0);

        if (mode == '1') {
            BasicCalculator stdcalc=new StandardCalculator();
            System.out.println("enter two no");

            double a = input.nextDouble();
            double b = input.nextDouble();
            System.out.println("enter operator");
            String ch = input.next();


            switch (ch) {
                case "+":
                    double add;
                    add = stdcalc.Add(a, b);
                    System.out.println("Addition:" + add);
                    break;
                case "-":
                    double sub = stdcalc.subtraction(a, b);
                    System.out.println("subtraction:" + sub);
                    break;
                case "*":
                    double mult = stdcalc.Multiplication(a, b);
                    System.out.println("multiplication:" + mult);
                    break;
                case "/":
                    double div = stdcalc.Divison(a, b);
                    System.out.println("division:" + div);
                    break;
                case "c":
                    int mem = (int) stdcalc.clearMemory();
                    System.out.println("clear memory " + mem);
                    break;
                case "%":
                    if(a>b){
                    int mod= (int) stdcalc.Modulus(a,b);
                    System.out.println("Modulus :" +mod);
                    break;
                    }
                    else{

                        int mod= (int) stdcalc.Modulus(b,a);
                        System.out.println("Modulus :" +mod);
                        break;
                    }

                default:
                    System.out.println("Invalid operator: "+ch);
            }

        }
        else if(mode=='2') {
            BasicCalculator1 sciCal=new ScientificCalculator();

            System.out.println("enter operator");
            String ch = input.next();




            switch (ch) {

                case "+":
                    System.out.println("enter two no");

                    double a = input.nextDouble();
                    double b = input.nextDouble();

                    double add = sciCal.Add(a, b);
                    sciCal.setMemory(add);
                    System.out.println(sciCal.getMemory());
                    System.out.println("Addition:" + add);
                    break;
                case "-":
                    System.out.println("enter two no");

                    a = input.nextDouble();
                    b = input.nextDouble();
                    double sub = sciCal.subtraction(a, b);
                    sciCal.setMemory(sub);
                    System.out.println(sciCal.getMemory());
                    System.out.println("subtraction:" + sub);

                    break;
                case "*":
                    System.out.println("enter two no");

                    a = input.nextDouble();
                    b = input.nextDouble();
                    double mult = sciCal.Multiplication(a, b);
                    sciCal.setMemory(mult);
                    System.out.println(sciCal.getMemory());
                    System.out.println("multiplication:" + mult);
                    break;
                case "/":
                    System.out.println("enter two no");

                    a = input.nextDouble();
                    b = input.nextDouble();
                    double div = sciCal.Divison(a, b);
                    sciCal.setMemory(div);
                    System.out.println(sciCal.getMemory());
                    System.out.println("division:" + div);
                    break;
                case "c":
                    int mem = (int) sciCal.clearMemory();
                    System.out.println("clear memory " + mem);
                    break;
                case "%":
                    System.out.println("enter two no");

                    a = input.nextDouble();
                    b = input.nextDouble();
                    if(a>b){
                        int mod= (int) sciCal.Modulus(a,b);
                        System.out.println("Modulus :" +mod);
                        break;
                    }
                    else{

                        int mod= (int) sciCal.Modulus(b,a);
                        System.out.println("Modulus :" +mod);
                        break;
                    }
                case "root":
                    System.out.println("enter no");

                    a = input.nextDouble();
                    b=input.nextDouble();

                    double nroot= sciCal.NthRoot(a,b);
                    System.out.println("squareroot :" +nroot);
                    break;
                case "power":
                    System.out.println("enter two no");
                    a = input.nextDouble();
                    b=input.nextDouble();
                    double npower=sciCal.pow(a,b);
                    System.out.println("nthpow: " +npower);
                    break;
                case "sin":
                    System.out.println("enter no");

                    a = input.nextDouble();
                    double sinval=sciCal.sin(a);
                    System.out.println("sin :" +sinval);
                    break;
                case "cos":
                    System.out.println("enter no");

                    a = input.nextDouble();
                    double cosval=sciCal.cos(a);
                    System.out.println("cos : " +cosval);
                    break;
                case "tan":
                    System.out.println("enter no");

                    a = input.nextDouble();
                    double tanval=sciCal.tan(a);
                    System.out.println("tan : " +tanval);
                    break;
                case "log":
                    System.out.println("enter no");

                    a = input.nextDouble();
                    double logval=sciCal.log(a);
                    System.out.println("log: " +logval);
                    break;

                default:
                    System.out.println("Invalid operator:" +ch);
            }

        }
        else{
            System.out.println("enter expression");
            Scanner input1=new Scanner(System.in);
            String exp=input1.nextLine();
            int n= GenCalci.evaluate(exp);
            System.out.println(n);

        }


    }
}
